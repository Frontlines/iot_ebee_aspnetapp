﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IoT_eBee.Models
{
    public class Sensor
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [StringLength(40, MinimumLength = 3)]
        public string SensorId { get; set; }
        [StringLength(450, MinimumLength = 3)]
        public string UserId { get; set; }
        [StringLength(60, MinimumLength = 3)]
        public string Nickname { get; set; }

        public Sensor()
        {
            Nickname = "Temporary";
        }

        public virtual ICollection<SensorData> SensorDatas { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
