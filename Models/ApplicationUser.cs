﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace IoT_eBee.Models
{
    public class ApplicationUser : IdentityUser
    {
        public virtual ICollection<Sensor> Sensors { get; set; }
    }
}
