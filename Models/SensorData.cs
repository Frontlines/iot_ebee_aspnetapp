﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IoT_eBee.Models
{
    public class SensorData
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [StringLength(40, MinimumLength = 3)]
        public string SensorDataId { get; set; }
        [StringLength(40, MinimumLength = 3)]
        public string SensorId { get; set; }
        // timestamp is saved as UNIX timestamp - seconds from 1.1.1970
        public long Timestamp { get; set; }
        public double Temperature { get; set; }
        public double Humidity { get; set; }
        public double Weight { get; set; }

        public SensorData()
        {
            Timestamp = DateTimeOffset.UtcNow.ToUnixTimeSeconds();
        }

        public virtual Sensor Sensor { get; set; }
    }
}
