﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace IoT_eBee.Migrations
{
    public partial class ModelCreatChange : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SensorDatas_Sensors_SensorId",
                table: "SensorDatas");

            migrationBuilder.AlterColumn<string>(
                name: "SensorId",
                table: "SensorDatas",
                maxLength: 40,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(40)",
                oldMaxLength: 40);

            migrationBuilder.AddForeignKey(
                name: "FK_SensorDatas_Sensors_SensorId",
                table: "SensorDatas",
                column: "SensorId",
                principalTable: "Sensors",
                principalColumn: "SensorId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SensorDatas_Sensors_SensorId",
                table: "SensorDatas");

            migrationBuilder.AlterColumn<string>(
                name: "SensorId",
                table: "SensorDatas",
                type: "nvarchar(40)",
                maxLength: 40,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 40,
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_SensorDatas_Sensors_SensorId",
                table: "SensorDatas",
                column: "SensorId",
                principalTable: "Sensors",
                principalColumn: "SensorId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
