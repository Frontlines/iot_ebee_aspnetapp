﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using IoT_eBee.Data;
using IoT_eBee.Models;
using Newtonsoft.Json;

namespace IoT_eBee.Controllers.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class SensorController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public SensorController(ApplicationDbContext context)
        {
            _context = context;
        }

        //GET: api/Sensors
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Sensor>>> GetSensors(string uid)
        {
            return await _context.Sensors.ToListAsync();
        }

        // GET: api/Sensors/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Sensor>> GetSensor(string id)
        {
            var sensor = await _context.Sensors.FindAsync(id);

            if (sensor == null)
            {
                return NotFound();
            }

            return sensor;
        }

        // POST: api/Sensors
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<JsonResult> PostSensor()
        {
            var sensor = new Sensor();
            _context.Sensors.Add(sensor);
            await _context.SaveChangesAsync();

            Dictionary<string, string> sensorInfo = new Dictionary<string, string>
            {
                { "SensorId", sensor.SensorId }
            };

            JsonResult result = new JsonResult(sensorInfo);
            return result;
        }

        private bool SensorExists(string id)
        {
            return _context.Sensors.Any(e => e.SensorId == id);
        }
    }
}
