﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using IoT_eBee.Data;
using IoT_eBee.Models;

namespace IoT_eBee.Controllers.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class SensorDataController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public SensorDataController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/SensorData
        //[HttpGet]
        //public async Task<ActionResult<IEnumerable<SensorData>>> GetSensorDatas()
        //{
        //    return await _context.SensorDatas.ToListAsync();
        //}

        // GET: api/SensorData/5
        [HttpGet("{id}")]
        public async Task<ActionResult<SensorData>> GetSensorData(string id)
        {
            var sensorData = await _context.SensorDatas.FindAsync(id);

            if (sensorData == null)
            {
                return NotFound();
            }

            return sensorData;
        }

        // PUT: api/SensorData/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSensorData(string id, SensorData sensorData)
        {
            if (id != sensorData.SensorDataId)
            {
                return BadRequest();
            }

            _context.Entry(sensorData).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SensorDataExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/SensorData
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult> PostSensorData(SensorData sensorData)
        {
            _context.SensorDatas.Add(sensorData);
            await _context.SaveChangesAsync();

            return Ok();
        }

        private bool SensorDataExists(string id)
        {
            return _context.SensorDatas.Any(e => e.SensorDataId == id);
        }
    }
}
